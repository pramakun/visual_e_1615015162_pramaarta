-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2018 at 05:50 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `praktikum_visual`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `awal` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama`, `tipe`, `awal`, `diskon`, `harga`) VALUES
(2, 'Carambit', 'Reguler', 100000, 24, 76000),
(3, 'Carambit 2', 'Exclusive', 120000, 51, 118800);

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id` int(10) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `harga` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id`, `judul`, `penulis`, `harga`) VALUES
(1, 'Honkai', 'Kiana Kaslana', 227927),
(2, '12', 'Theresa Apocalypse', 12),
(4, 'eiae', 'Bronya Zaychik', 818181),
(5, 'qwe', 'Theresa Apocalypse', 1),
(7, 'Ini Judul', 'Raiden Mei', 90000),
(8, 'Sayang Ibu', 'Raiden Mei', 10000),
(10, 'qwerty', 'Theresa Apocalypse', 12000),
(11, 'Buku Barru', 'Murata Himeko', 2000),
(12, 'Power Point', 'Bronya Zaychik', 90000),
(13, 'Ricis Nabati', 'Bronya Zaychik', 12000),
(14, 'ABC', 'Raiden Mei', 111111),
(15, 'ABC', 'Raiden Mei', 111111),
(16, 'Honkai 2', 'Kiana Kaslana', 227927),
(17, 'Honkai 2', 'Kiana Kaslana', 227927),
(19, 'Sayang Ibu', 'Prama Arta', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `meh`
--

CREATE TABLE `meh` (
  `id` int(10) NOT NULL,
  `penulis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meh`
--

INSERT INTO `meh` (`id`, `penulis`) VALUES
(1, 'Theresa Apocalypse'),
(2, 'Raiden Mei'),
(3, 'Kiana Kaslana'),
(4, 'Murata Himeko'),
(5, 'Bronya Zaychik'),
(6, 'Rahadi Riyanto'),
(10, 'Prama Arta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meh`
--
ALTER TABLE `meh`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
